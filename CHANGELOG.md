##  CHANGELOG  ##

1.6.0:
- slight changes to output of "--help" and "-h" variables
- Capital Y in question to install nvidia drivers
- added check for dnf5
- implemented runCommand in a more secure way
- added config option to start a specific version of the program by default
- replace our neofetch fork with fastfetch
- added creator install
- added Fedora 40 support
- made read processes more advanced
- removed char command[256] and char customoption[256] because they aren't in use anymore
- activate fedora-cisco-openh264 everywhere needed
- added distro detection
- easier choosing what commands are run

1.6.1:
- search bug fixed
- unified options
- removed Fedora 38 support

1.6.2:
- fixed copr enabling bug
- fixed displaying flatpak commands
- fixed displaying dnf commands
- added new function "runCommandNew"
- using runCommandNew instead of runCommand

1.7.0:
- moved all functions to [libstp](https://gitlab.com/setup-tooling-project/libstp)
- removing old deprecated runCommand function
- added basic logging
- added CentOS Stream 10 support
- added Fedora 41 support
- changing booleans to ints 
- fastfetch reexecuting program fixed by fastfetch-cli project in version [2.22.0](https://github.com/fastfetch-cli/fastfetch/releases/tag/2.22.0)
- added arm 64bit support
- added password prompt back
- added compatibility for [community scripts](https://gitlab.com/setup-tooling-project/st-community-scripts)
- fixed snap installation bug
- added notice for aarch64 in GPU search

1.7.1:
- set the config file (due to change in [libstp 1.1.3](https://gitlab.com/setup-tooling-project/libstp/-/tree/1.1.3?ref_type=tags))
- fixed bug in -msd/--make-script-directory flag

1.7.2:
- made changes to work with [libstp 1.2.0](https://gitlab.com/setup-tooling-project/libstp/-/tree/1.2.0?ref_type=tags)

1.7.2-2:
- Christmas patch

1.7.3:
- moved Christmas and New Year's to libstp

1.7.4:
- made changes due to libstp changes