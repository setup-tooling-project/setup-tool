# Setup Tool

<a target="_blank" href="https://opencollective.com/setup-tooling-project"><img src="https://opencollective.com/setup-tooling-project/total/badge.svg?label=Open%20Collective%20Backers&color=brightgreen" /></a>
______________________________________________________

This project aims to help users to setup and configure their rpm-based Linux systems more easily.
This includes but is not limited to setting up additional repositories and configuring dnf.
______________________________________________________

Please read the README.md on older versions, if you use those, due to potential differences in the features supported by different versions.
______________________________________________________

supported Fedora versions
- [x] 39        (old) (only for upgrades to newer Fedora versions)
- [x] 40        (current)
- [x] 41        (current)

______________________________________________________

supported RHEL versions
- [x] 9.x        (current)
- [ ] 10.x       (future)  (experimental)

______________________________________________________

supported CentOS versions
- [x] Stream 9
- [x] Stream 10

______________________________________________________

supported versions
- [x] 1.7.x       (stable)

______________________________________________________

supported variants
- [x] CLI
- [ ] ~TUI~ (until 1.4.103c)
- [ ] GUI (coming sometime)

______________________________________________________

start page options:
- repo setup
- DNF modification
- Codec Setup 
- package management
- premade scripts
- system information
- system updates (dnf update & flatpak update & snap refresh)
- system upgrade
- reboot system
- community scripts

______________________________________________________

Repo Setup gives you the options to install:
- RPM Fusion free (repo)
- RPM Fusion nonfree (repo)
- RPM Fusion free tainted (repo)
- RPM Fusion nonfree tainted (repo)
- Flathub (repo) (not needed if third party repos are enabled)
- all of the above
- snapd
- everything above including snapd
- bring your own COPR

______________________________________________________

DNF modification tool options:
- set default answer to Yes/Y
- set maximum simultaneous downloads to 10
- bring in your own dnf.conf changes
______________________________________________________

Codec Setup options:
- additional general multimedia codecs
- libdvdcss/DVD compatibility
- GPU specific

_______________________________________________________

Package Management Helper:
- install packages
- remove packages
- search for packages

______________________________________________________

Premade scripts:
- Gaming
- Streaming
- Vax (Hyprland)
- Creator Conf
- Office

______________________________________________________

System info:
- fastfetch/neofetch
- lspci -nnk
- lsusb
- lsblk -fa

______________________________________________________

NOW AVAILABLE ON COPR!!!

sudo dnf copr enable burningpho3nix/Setup-Tool -y

stable:
sudo dnf install setup-tool -y

beta:
sudo dnf install setup-tool-beta -y

_______________________________________________________

DISCLAIMER

THIS PROJECT HAS NO RELATION TO THE FEDORA PROJECT NOR THE RPM FUSION PROJECT NOR TO FLATHUB NOR TO SNAPCRAFT.
FURTHERMORE THIS PROJECT HAS NO RELATION TO CANONICAL LTD., RED HAT, RED HAT CZECH, RED HAT INDIA AND IBM.

If you have any problems with software from "The Fedora Project", "RPM-Fusion", "Flathub", Canonical Ltd., "Snapcraft", any "Red Hat" Company or IBM,
please report the issue to them.

If you have issues with this program report those to us through the "[Issues](https://gitlab.com/setup-tooling-project/setup-tool/-/issues)" category on GitLab.
