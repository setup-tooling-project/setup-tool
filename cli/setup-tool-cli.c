#include <stdio.h>                                                                              // libraries
#include <stdlib.h>                                                                             // that
#include <string.h>                                                                             // are
#include <sys/syslog.h>
#include <unistd.h>                                                                             // needed
#include <termios.h>                                                                            // for
#include <stdbool.h>                                                                            // this
#include <fcntl.h>                                                                              // to run
#include <time.h>
#include <sys/wait.h>
#include <syslog.h>
#include <dirent.h>
#include <sys/stat.h>

#include "/usr/lib64/libstp/libstp-c.h"

#define MAX_SCRIPTS 128

int main(int argc, char *argv[]) {                                        // main function containing all interactive and user shown code
    if (argc == 2 && strcmp(argv[1], "--version") == 0 || argc == 2 && strcmp(argv[1], "-v") == 0) {
        runCommand("rpm -q setup-tool-cli");
    } else if (argc == 2 && strcmp(argv[1], "--arch") == 0 || argc == 2 && strcmp(argv[1], "-a") == 0) {
       runCommand("uname -m");
    } else if (argc == 2 && strcmp(argv[1], "--make-script-directory") == 0 || argc == 2 && strcmp(argv[1], "-msd") == 0) {
       const char *home_dir = getenv("HOME");
        if (home_dir == NULL) {
            fprintf(stderr, "Error: Unable to determine home directory.\n");
            return 1;
        }

        char script_dir[MAX_PATH];
        int result = snprintf(script_dir, sizeof(script_dir), "%s/.local/share/setup-tool/community-scripts", home_dir);
        if (result < 0 || result >= (int)sizeof(script_dir)) {
            fprintf(stderr, "Error: Path construction failed.\n");
            return 1;
        }

        struct stat st;
        if (stat(script_dir, &st) == 0) {
            printf("Directory already exists: %s\n", script_dir);
        } else {
            char mkdir_command[MAX_PATH * 2];
            snprintf(mkdir_command, sizeof(mkdir_command), "mkdir -p %s", script_dir);
            if (system(mkdir_command) == -1) {
                perror("Error creating directory");
                return 1;
            } else {
                printf("Directory created successfully: %s\n", script_dir);
            }
        }
    } else if (argc == 2 && strcmp(argv[1], "--help") == 0 || argc == 2 && strcmp(argv[1], "-h") == 0) {
        printf("Setup Tool help page\n");
        printf("\n");
        printf("Usage:\n");
        printf("\n");
        printf("--version/-v                    | for the version\n");
        printf("--arch/-a                       | for your CPU architecture\n");
        printf("--make-script-directory/-msd    | to make the script directory (~/.local/share/setup-tool/community-scripts)\n");
        printf("--help/-h                       | for this help page\n");
        printf("\n");
        printf("If used without flags it will start normal\n");
        printf("\n");
    } else {

    openlog("setup-tool", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);

    syslog(LOG_INFO, "-----START OF LOG-----");

    setConfigFile("/etc/setup-tool.conf");

    syslog(LOG_INFO, "config file set");

    char* real_user = get_real_user();

    int option;

    char packageName[256];
    char sudoCommand[256];
    char copr[256];
    char coprCommand[256];
    char linux_distro[128];
    char answer[2];
    char version[20];

    char* version_stp;
    char* password = getSecurePasswordSTP();

    syslog(LOG_INFO, "set arrays");

    extractOSVersion(version);
    extractLinuxDistro(linux_distro);
    strlwr(linux_distro);

    version_stp = getVersion("setup-tool-cli");

    syslog(LOG_INFO, "got distro and distro version");

    if (password) {
        char *sudoArgs[] = {"sudo", "-S", "clear", NULL};
        int pipefd[2];
        if (pipe(pipefd) == -1) {
          perror("Error creating pipe");
          return 1;
        }
        pid_t pid = fork();
        if (pid == -1) {
          perror("Error forking");
          return 1;
        } else if (pid == 0) { // Child process
          close(pipefd[1]);
          dup2(pipefd[0], STDIN_FILENO);
          close(pipefd[0]);
          execvp(sudoArgs[0], sudoArgs);
          perror("Error executing sudo");
          return 1;
        } else {
          close(pipefd[0]);
          write(pipefd[1], password, strlen(password));
          write(pipefd[1], "\n", 1);
          close(pipefd[1]);
          wait(NULL);
        }
        free(password);
    }

    time_t t = time(NULL);
    struct tm *current_time = localtime(&t);
    int month = current_time->tm_mon + 1;    // tm_mon is 0-11, so add 1
    int day = current_time->tm_mday;

    display_special_day();

    if (shouldShowNotice_beta()) {
        printf("This version of the Setup Tool is still not final.\n");
        printf("\n");
    }

    int fi = system("flatpak --version > /dev/null 2>&1");
    int si = system("snap --version > /dev/null 2>&1");
    int di = system("dnf --version > /dev/null 2>&1");
    int ffi = system("fastfetch --version > /dev/null 2>&1");
    int stcs_pre = system("rpm -q st-community-scripts > /dev/null 2>&1");

    printf("Be careful this is running as administrator.\n");
    printf("\n");

    int amd = system("lspci -nnk | grep -EA3 'VGA|3D' | grep 'AMD/ATI' > /dev/null 2>&1");
    int nv = system("lspci -nnk | grep -EA3 'VGA|3D' | grep 'nvidia' > /dev/null 2>&1");
    int intel = system("lspci -nnk | grep -EA3 'VGA|3D' | grep 'Intel' > /dev/null 2>&1");

    syslog(LOG_INFO, "running as admin");

    printf("Linux Distribution: %s\n", linux_distro);
    printf("OS version: %s\n", version);

    syslog(LOG_INFO, "Current version: %s", version_stp);

    int arch_amd64 = system("uname -m | grep 'x86_64' > /dev/null 2>&1");
    int arch_aarch64 = system("uname -m | grep 'aarch64' > /dev/null 2>&1");

    syslog(LOG_INFO, "Getting GPU info");

    int driverCheck = system("lsmod | grep nvidia");

    if (amd == 0){
        printf("GPU Vendor: AMD\n");
        syslog(LOG_INFO, "GPU: AMD");
    }
    if (nv == 0) {
        printf("GPU Vendor: Nvidia\n");
        syslog(LOG_INFO, "GPU: Nvidia");
    }
    if (intel == 0) {
        printf("GPU Vendor: Intel\n");
        syslog(LOG_INFO, "GPU: Intel");
    }
    if (amd != 0 && nv != 0 && intel != 0){
        if (arch_aarch64 == 0) {
            printf("GPU Vendor: The GPU detection is not supported on this architecture yet.\n");
            printf("We are working on adding support for this.\n");
            printf("You can support our efforts on this, here: https://bit.ly/oc-stp \n");
            syslog(LOG_INFO, "User is on aarch64");
        } else if (arch_amd64 == 0) {
            printf("GPU Vendor: Unknown or GPU not found\n");
            syslog(LOG_INFO, "GPU not known or not existing");
        }
    }

    if (fi != 0 && si != 0 && di != 0) {
    printf("Flatpak, Snap, and DNF package managers are not installed on this system.\n");
    printf("Please install at least one of them to use the Package Management Helper.\n");
    }

    if (stcs_pre != 0) {
        char confirm[2];
        int validInput = 0;

        while (!validInput) {
            printf("Do you want to install the community scripts package? (Y/n): ");
            fgets(confirm, sizeof(confirm), stdin);
            if (confirm[0] == 'Y' || confirm[0] == 'y' || confirm[0] == '\n') {
                runCommand("sudo dnf install st-community-scripts -y");
                runCommand("clear");
                if (shouldShowNotice_beta()) {
                    printf("This version of the Setup Tool is still not final.\n");
                    printf("\n");
                }
                    printf("Be careful this is running as administrator.\n");
                    printf("\n");

                    printf("Linux Distribution: %s\n", linux_distro);
                    printf("OS version: %s\n", version);

                if (amd == 0){
                    printf("GPU Vendor: AMD\n");
                    syslog(LOG_INFO, "GPU: AMD");
                }
                if (nv == 0) {
                    printf("GPU Vendor: Nvidia\n");
                    syslog(LOG_INFO, "GPU: Nvidia");
                }
                if (intel == 0) {
                    printf("GPU Vendor: Intel\n");
                    syslog(LOG_INFO, "GPU: Intel");
                }
                if (amd != 0 && nv != 0 && intel != 0){
                    if (arch_aarch64 == 0) {
                        printf("GPU Vendor: The GPU detection is not supported on this architecture yet.\n");
                        printf("We are working on adding support for this.\n");
                        printf("You can support our efforts on this, here: https://bit.ly/oc-stp \n");
                        syslog(LOG_INFO, "User is on aarch64");
                    } else if (arch_amd64 == 0) {
                        printf("GPU Vendor: Unknown or GPU not found\n");
                        syslog(LOG_INFO, "GPU not known or not existing");
                    }
                }
                break;
            } else {
                break;
            }
        }
    }
    int stcs = system("rpm -q st-community-scripts > /dev/null 2>&1");

        while (1) {
            printf("\n");
            printf("Choose an option:\n");
            printf("1. Repo Setup\n");
            printf("2. DNF Modification Tool\n");
            printf("3. Codec Setup\n");
            printf("4. Package Management Helper\n");
            printf("5. Premade Scripts\n");
            printf("6. System Info\n");
            printf("7. Update\n");
            printf("8. Systemupgrade\n");
            printf("9. Reboot\n");
            if(stcs == 0){
                printf("10. Community Scripts\n");
                printf("11. Quit\n");
                printf("Option: ");
                option = getMenuOption(11);
            } else if (stcs != 0){
                printf("10. Quit\n");
                printf("Option: ");
                option = getMenuOption(10);
            }

            syslog(LOG_INFO, "Displaying main menu");

        switch (option) {
            case 1: {
                int repo_option;
                while (1) {
                    printf("\n");
                    printf("Choose an option for Repo Setup:\n");
                    printf("1. Install RPM Fusion Free\n");
                    printf("2. Install RPM Fusion Free Tainted\n");
                    printf("3. Install RPM Fusion Non-Free\n");
                    printf("4. Install RPM Fusion Non-Free Tainted\n");
                    printf("5. Install Flathub\n");
                    printf("6. Install All\n");
                    printf("7. Install and enable snapd\n");
                    printf("8. Install All + snapd\n");
                    printf("9. Bring your own Copr\n");
                    printf("10. Back to main menu\n");
                    printf("Option: ");
                    repo_option = getMenuOption(10);

                    syslog(LOG_INFO, "Displaying repo menu");

                    switch (repo_option) {
                        case 1:
                            if (strcmp(linux_distro, "fedora") == 0) {
                            runCommand("sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %%fedora).noarch.rpm");
                            runCommand("sudo dnf config-manager --enable fedora-cisco-openh264");
                            if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                            } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                            }
                            } else if (strcmp(linux_distro, "rhel") == 0 ||
                                    strcmp(linux_distro, "centos") == 0 ||  // Includes CentOS Stream
                                    strcmp(linux_distro, "oracle") == 0 ||
                                    strstr(linux_distro, "rhel") != NULL ||
                                    strstr(linux_distro, "centos") != NULL ||
                                    strstr(linux_distro, "oracle") != NULL){
                            runCommand("sudo dnf install -y --nogpgcheck https://dl.fedoraproject.org/pub/epel/epel-release-latest-$(rpm -E %%rhel).noarch.rpm");
                            runCommand("sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %%rhel).noarch.rpm");
                            if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                            } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                            }
                            }
                            syslog(LOG_INFO, "Running Option 1");
                            break;
                        case 2:
                            runCommand("sudo dnf install -y rpmfusion-free-release-tainted");
                            syslog(LOG_INFO, "Running Option 2");
                            break;
                        case 3:
                        if (strcmp(linux_distro, "fedora") == 0) {
                            runCommand("sudo dnf install -y https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %%fedora).noarch.rpm");
                            runCommand("sudo dnf config-manager --enable fedora-cisco-openh264");
                            if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                            } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                            }
                        } else if (strcmp(linux_distro, "rhel") == 0 ||
                                   strcmp(linux_distro, "centos") == 0 ||  // Includes CentOS Stream
                                   strcmp(linux_distro, "oracle") == 0 ||
                                   strstr(linux_distro, "rhel") != NULL ||
                                   strstr(linux_distro, "centos") != NULL ||
                                   strstr(linux_distro, "oracle") != NULL){
                            runCommand("sudo dnf install -y --nogpgcheck https://dl.fedoraproject.org/pub/epel/epel-release-latest-$(rpm -E %%rhel).noarch.rpm");
                            runCommand("sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-$(rpm -E %%rhel).noarch.rpm");
                            if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                            } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                            }
                        }
                            syslog(LOG_INFO, "Running Option 3");
                            break;
                        case 4:
                            runCommand("sudo dnf install -y rpmfusion-nonfree-release-tainted");
                            syslog(LOG_INFO, "Running Option 4");
                            break;
                        case 5:
                            runCommand("flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo");
                            syslog(LOG_INFO, "Running Option 5");
                            break;
                        case 6:
                            if (strcmp(linux_distro, "fedora") == 0) {
                            runCommand("sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %%fedora).noarch.rpm");
                            runCommand("sudo dnf install -y https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %%fedora).noarch.rpm");
                            runCommand("sudo dnf config-manager --enable fedora-cisco-openh264");
                            if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                            } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                            }
                        } else if (strcmp(linux_distro, "rhel") == 0 ||
                                   strcmp(linux_distro, "centos") == 0 ||  // Includes CentOS Stream
                                   strcmp(linux_distro, "oracle") == 0 ||
                                   strstr(linux_distro, "rhel") != NULL ||
                                   strstr(linux_distro, "centos") != NULL ||
                                   strstr(linux_distro, "oracle") != NULL){
                            runCommand("sudo dnf install -y --nogpgcheck https://dl.fedoraproject.org/pub/epel/epel-release-latest-$(rpm -E %%rhel).noarch.rpm");
                            runCommand("sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %%rhel).noarch.rpm");
                            runCommand("sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-$(rpm -E %%rhel).noarch.rpm");
                            if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                            } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                            }
                        }
                            runCommand("sudo dnf install -y rpmfusion-free-release-tainted");
                            runCommand("sudo dnf install -y rpmfusion-nonfree-release-tainted");
                            runCommand("flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo");
                            syslog(LOG_INFO, "Running Option 6");
                            break;
                        case 7:
                            runCommand("sudo dnf install -y snapd");
                            runCommand("sudo ln -s /var/lib/snapd/snap /snap");
                            syslog(LOG_INFO, "Running Option 7");
                            break;
                        case 8:
                            if (strcmp(linux_distro, "fedora") == 0) {
                            runCommand("sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %%fedora).noarch.rpm");
                            runCommand("sudo dnf install -y https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %%fedora).noarch.rpm");
                            runCommand("sudo dnf config-manager --enable fedora-cisco-openh264");
                            if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                            } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                            }
                        } else if (strcmp(linux_distro, "rhel") == 0 ||
                                   strcmp(linux_distro, "centos") == 0 ||  // Includes CentOS Stream
                                   strcmp(linux_distro, "oracle") == 0 ||
                                   strstr(linux_distro, "rhel") != NULL ||
                                   strstr(linux_distro, "centos") != NULL ||
                                   strstr(linux_distro, "oracle") != NULL){
                            runCommand("sudo dnf install -y --nogpgcheck https://dl.fedoraproject.org/pub/epel/epel-release-latest-$(rpm -E %%rhel).noarch.rpm");
                            runCommand("sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %%rhel).noarch.rpm");
                            runCommand("sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-$(rpm -E %%rhel).noarch.rpm");
                            if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                            } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                            }
                        }
                            runCommand("sudo dnf install -y rpmfusion-free-release-tainted");
                            runCommand("sudo dnf install -y rpmfusion-nonfree-release-tainted");
                            runCommand("sudo dnf install -y snapd");
                            runCommand("sudo ln -s /var/lib/snapd/snap /snap");
                            runCommand("flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo");
                            syslog(LOG_INFO, "Running Option 8");
                            break;
                        case 9:
                            printf("Copr name (creator/Program): ");
                            scanf("%s", copr);
                            sprintf(coprCommand, "sudo dnf copr enable -y %s", copr);
                            int result = system(coprCommand);
                            syslog(LOG_INFO, "Running Option 9");
                            break;
                        case 10:
                            printf("Returning to main menu.\n");
                            syslog(LOG_INFO, "Returning to main menu");
                            break;
                        default:
                            printf("Invalid option. Please try again.\n");
                            break;
                    }

                    if (repo_option == 10) {
                        break;
                    }
                }
                break;
            }
            case 2: {
                int dnf_option;
                while (1) {
                    printf("\n");
                    printf("Choose an option for DNF modification tool:\n");
                    printf("1. Set defaultyes=True\n");
                    printf("2. Set max_parallel_downloads=10\n");
                    printf("3. Set all recommended options\n");
                    printf("4. Add custom option\n");
                    printf("5. Back to main menu\n");
                    printf("Option: ");
                    dnf_option = getMenuOption(5);

                    syslog(LOG_INFO, "Displaying DNF menu");

                    switch (dnf_option) {
                        case 1:
                            if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf config-manager --setopt=\"defaultyes=True\" --save");
                            } else if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf config-manager setopt defaultyes=True");
                            }
                            syslog(LOG_INFO, "Running Option 1");
                            break;
                        case 2:
                            if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf config-manager --setopt=\"max_parallel_downloads=10\" --save");
                            } else if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf config-manager setopt max_parallel_downloads=10");
                            }
                            syslog(LOG_INFO, "Running Option 2");
                            break;
                        case 3:
                            if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf config-manager --setopt=\"defaultyes=True\" --setopt=\"max_parallel_downloads=10\" --save");
                            } else if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf config-manager setopt defaultyes=True max_parallel_downloads=10");
                            }
                            syslog(LOG_INFO, "Running Option 3");
                            break;
                        case 4: {
                            char custom_option[256];
                            printf("Enter the custom option: ");
                            scanf("%s", custom_option);

                            char command[256];
                            if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                snprintf(command, sizeof(command), "sudo dnf config-manager --setopt=\"%s\" --save", custom_option);
                            } else if (strcmp(version, "41") == 0) {
                                snprintf(command, sizeof(command), "sudo dnf config-manager setopt %s", custom_option);
                            }

                            runCommand(command);
                            syslog(LOG_INFO, "Running Option 4");
                            break;
                        }
                        case 5:
                            printf("Returning to main menu.\n");
                            syslog(LOG_INFO, "Returning to main menu");
                            break;
                        default:
                            printf("Invalid option. Please try again.\n");
                            break;
                    }

                    if (dnf_option == 5) {
                        break;
                    }
                }
                break;
            }
            case 3: {
                int codec_option;
                while (1) {
                    printf("\n");
                    printf("Choose an option for Codec Support:\n");
                    printf("1. Install general Codecs\n");
                    printf("2. DVD support\n");
                    printf("3. GPU specific\n");
                    printf("4. Back to main menu\n");
                    printf("Option: ");
                    codec_option = getMenuOption(4);

                    syslog(LOG_INFO, "Displaying Codec menu");

                    switch (codec_option) {
                        case 1:
                            runCommand("sudo dnf swap -y ffmpeg-free ffmpeg --allowerasing");
                            if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y multimedia --setop=\"install_weak_deps=False\" --exclude=PackageKit-gstreamer-plugin");
                                runCommand("sudo dnf groupupdate -y sound-and-video");
                            } else if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y multimedia -x PackageKit-gstreamer-plugin");
                                runCommand("sudo dnf group upgrade -y sound-and-video");
                            }
                            runCommand("sudo dnf --repo=rpmfusion-nonfree-tainted install \"*-firmware\" -y");
                            syslog(LOG_INFO, "Running Option 1");
                            break;
                        case 2:
                            runCommand("sudo dnf install -y libdvdcss");
                            syslog(LOG_INFO, "Running Option 2");
                            break;
                        case 3:
                            if(amd == 0){
                            runCommand("sudo dnf swap -y mesa-va-drivers mesa-va-drivers-freeworld");
                            runCommand("sudo dnf swap -y mesa-vdpau-drivers mesa-vdpau-drivers-freeworld");
                            runCommand("sudo dnf swap -y mesa-va-drivers.i686 mesa-va-drivers-freeworld.i686");
                            runCommand("sudo dnf swap -y mesa-vdpau-drivers.i686 mesa-vdpau-drivers-freeworld.i686");
                            }
                            if(nv == 0){
                            runCommand("sudo dnf install akmod-nvidia nvidia-settings nvidia-modprobe nvtop nv-codec-headers -y");
                            runCommand("sudo dnf install -y libva-nvidia-driver");
                            }
                            if(intel == 0){
                            runCommand("sudo dnf install -y intel-media-driver");
                            }
                            syslog(LOG_INFO, "Running Option 3");
                            break;
                        case 4:
                            printf("Returning to main menu.\n");
                            syslog(LOG_INFO, "Returning to main menu");
                            break;
                        default:
                            printf("Invalid option. Please try again.\n");
                            break;
                    }

                    if (codec_option == 4) {
                        break;
                    }
                }
                break;
            }
            case 4: {

                int package_option;
                int package_option_loop = 1;
                while (package_option_loop) {
                    printf("\n");
                    printf("Choose an option for Package Management Helper:\n");
                    printf("1. Install a package\n");
                    printf("2. Remove a package\n");
                    printf("3. Search for a package\n");
                    printf("4. Back to main menu\n");
                    printf("Option: ");
                    package_option = getMenuOption(4);

                    syslog(LOG_INFO, "Displaying Package Management menu");

                    switch (package_option) {
                        case 1: {
                            printf("Enter the name of the package to install: ");
                            scanf(" %[^\n]", packageName);

                            bool installed = false; // Flag to track installation status

                            if (fi == 0) {
                                if (installWithPackageManager(packageName, "flatpak")) {
                                    printf("%s installed successfully using Flatpak.\n", packageName);
                                    installed = true;
                                }
                            }

                            if (!installed && di == 0) {
                                if (installWithPackageManager(packageName, "sudo dnf --refresh")) {
                                    printf("%s installed successfully using DNF.\n", packageName);
                                    installed = true;
                                }
                            }

                            if (!installed && si == 0) {
                                if (installWithPackageManager(packageName, "sudo snap")) {
                                    printf("%s installed successfully using Snap.\n", packageName);
                                }
                            }

                            if (!installed) {
                                if (fi != 0) {
                                    if (di != 0) {
                                        if (si != 0) {
                                        printf("Flatpak, DNF and Snap are not installed.\n");
                                        } else {
                                        printf("Flatpak and DNF are not installed.\n");
                                        }
                                    } else {
                                    printf("Flatpak is not installed.\n");
                                    }
                                } else if (di != 0) {
                                    if (si != 0) {
                                    printf("DNF and Snap are not installed.\n");
                                    } else {
                                    printf("DNF is not installed.\n");
                                    }
                                } else if (si != 0) {
                                    printf("Snap is not installed.\n");
                                } else {
                                    printf("The installation failed, please check your input.\n");
                                }
                            }
                            syslog(LOG_INFO, "Running Option 1");
                            break;
                        }
                        case 2: {
                            printf("Enter the name of the package to uninstall: ");
                            scanf(" %[^\n]", packageName);

                            bool uninstalled = false; // Flag to track uninstallation status

                            if (fi == 0) {
                                if (uninstallWithFlatpak(packageName)) {
                                    printf("%s uninstalled successfully using Flatpak.\n", packageName);
                                    uninstalled = true;
                                }
                            }

                            if (!uninstalled && di == 0) {
                                if (uninstallWithDNF(packageName)) {
                                    printf("%s uninstalled successfully using DNF.\n", packageName);
                                    uninstalled = true;
                                }
                            }

                            if (!uninstalled && si == 0) {
                                if (uninstallWithSnap(packageName)) {
                                    printf("%s uninstalled successfully using Snap.\n", packageName);
                                }
                            }

                            if (!uninstalled) {
                                if (fi != 0) {
                                    if (di != 0) {
                                        if (si != 0) {
                                        printf("Flatpak, DNF and Snap are not installed.\n");
                                        } else {
                                        printf("Flatpak and DNF are not installed.\n");
                                        }
                                    } else {
                                    printf("Flatpak is not installed.\n");
                                    }
                                } else if (di != 0) {
                                    if (si != 0) {
                                    printf("DNF and Snap are not installed.\n");
                                    } else {
                                    printf("DNF is not installed.\n");
                                    }
                                } else if (si != 0) {
                                    printf("Snap is not installed.\n");
                                } else {
                                    printf("The uninstallation failed, please check your input.\n");
                                }
                            }
                            syslog(LOG_INFO, "Running Option 2");
                        break;
                        }
                        case 3: {
                            printf("Enter the name of the package to search: ");
                            scanf(" %[^\n]", packageName);

                            bool found = false; // Flag to track uninstallation status

                            if (fi == 0) {
                                if (searchWithFlatpak(packageName)) {
                                    printf("\n");
                                    printf("\n");
                                }
                            }

                            if (!found && di == 0) {
                                if (searchWithDNF(packageName)) {
                                    printf("\n");
                                    printf("\n");
                                }
                            }

                            if (!found && si == 0) {
                                if (searchWithSnap(packageName)) {
                                    printf("\n");
                                    printf("\n");
                                }
                            }
                            found = true;
                            syslog(LOG_INFO, "Running Option 3");
                        break;
                        }
                        case 4: {
                        // Return to the main menu
                        package_option_loop = false;
                        syslog(LOG_INFO, "Returning to main menu");
                        break;
                        }
                        default: {
                            printf("Invalid option. Please try again.\n");
                            break;
                        }
                    if (package_option == 4) {
                        package_option_loop = false;
                        break;
                     }
                }
            }
            break;
        }
            case 5: {
                int script_option;
                while (1) {
                    printf("\n");
                    printf("Choose an option for premade script:\n");
                    printf("1. Gaming\n");
                    printf("2. Streaming\n");
                    printf("3. Vax\n");
                    printf("4. Creator Install\n");
                    printf("5. Office\n");
                    printf("6. Back to main menu\n");
                    printf("Option: ");
                    script_option = getMenuOption(6);

                    syslog(LOG_INFO, "Displaying script menu");

                    switch (script_option) {
                        case 1:
                            if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf config-manager --setopt=\"defaultyes=True\" --setopt=\"max_parallel_downloads=10\" --save");
                            } else if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf config-manager setopt defaultyes=True max_parallel_downloads=10");
                            }
                            if (strcmp(linux_distro, "fedora") == 0) {
                                runCommand("sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %%fedora).noarch.rpm");
                                runCommand("sudo dnf install -y https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %%fedora).noarch.rpm");
                                runCommand("sudo dnf config-manager --enable fedora-cisco-openh264");
                               if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                            } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                            }
                            } else if (strcmp(linux_distro, "rhel") == 0 ||
                                strcmp(linux_distro, "centos") == 0 ||  // Includes CentOS Stream
                                strcmp(linux_distro, "oracle") == 0 ||
                                strstr(linux_distro, "rhel") != NULL ||
                                strstr(linux_distro, "centos") != NULL ||
                                strstr(linux_distro, "oracle") != NULL){
                            runCommand("sudo dnf install -y --nogpgcheck https://dl.fedoraproject.org/pub/epel/epel-release-latest-$(rpm -E %%rhel).noarch.rpm");
                            runCommand("sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %%rhel).noarch.rpm");
                            runCommand("sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-$(rpm -E %%rhel).noarch.rpm");
                            if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                            } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                            }
                                }
                            runCommand("sudo dnf install -y rpmfusion-free-release-tainted");
                            runCommand("sudo dnf install -y rpmfusion-nonfree-release-tainted");
                            runCommand("sudo dnf swap -y ffmpeg-free ffmpeg --allowerasing");
                            if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y multimedia --setop=\"install_weak_deps=False\" --exclude=PackageKit-gstreamer-plugin");
                                runCommand("sudo dnf groupupdate -y sound-and-video");
                            } else if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y multimedia -x PackageKit-gstreamer-plugin");
                                runCommand("sudo dnf group upgrade -y sound-and-video");
                            }
                            runCommand("sudo dnf --repo=rpmfusion-nonfree-tainted install \"*-firmware\" -y");
                            if(amd == 0){
                            runCommand("sudo dnf swap -y mesa-va-drivers mesa-va-drivers-freeworld");
                            runCommand("sudo dnf swap -y mesa-vdpau-drivers mesa-vdpau-drivers-freeworld");
                            runCommand("sudo dnf swap -y mesa-va-drivers.i686 mesa-va-drivers-freeworld.i686");
                            runCommand("sudo dnf swap -y mesa-vdpau-drivers.i686 mesa-vdpau-drivers-freeworld.i686");
                            }
                            if(nv == 0){
                            runCommand("sudo dnf install -y libva-nvidia-driver");
                            }
                            if(intel == 0){
                            runCommand("sudo dnf install -y intel-media-driver");
                            }
                            runCommand("sudo dnf install -y steam");
                            runCommand("flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo");
                            runCommand("flatpak install com.discordapp.Discord -y");
                            runCommand("flatpak install com.usebottles.bottles -y");
                            runCommand("flatpak install net.davidotek.pupgui2 -y");
                            runCommand("flatpak install net.lutris.Lutris -y");
                            syslog(LOG_INFO, "Running Option 1");
                            break;
                        case 2:
                            if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf config-manager --setopt=\"defaultyes=True\" --setopt=\"max_parallel_downloads=10\" --save");
                            } else if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf config-manager setopt defaultyes=True max_parallel_downloads=10");
                            }
                            if (strcmp(linux_distro, "fedora") == 0) {
                                runCommand("sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %%fedora).noarch.rpm");
                                runCommand("sudo dnf install -y https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %%fedora).noarch.rpm");
                                runCommand("sudo dnf config-manager --enable fedora-cisco-openh264");
                               if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                               } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                               }
                            } else if (strcmp(linux_distro, "rhel") == 0 ||
                                strcmp(linux_distro, "centos") == 0 ||  // Includes CentOS Stream
                                strcmp(linux_distro, "oracle") == 0 ||
                                strstr(linux_distro, "rhel") != NULL ||
                                strstr(linux_distro, "centos") != NULL ||
                                strstr(linux_distro, "oracle") != NULL){
                            runCommand("sudo dnf install -y --nogpgcheck https://dl.fedoraproject.org/pub/epel/epel-release-latest-$(rpm -E %%rhel).noarch.rpm");
                            runCommand("sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %%rhel).noarch.rpm");
                            runCommand("sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-$(rpm -E %%rhel).noarch.rpm");
                            if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                            } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                            }
                                }
                            runCommand("sudo dnf install -y rpmfusion-free-release-tainted");
                            runCommand("sudo dnf install -y rpmfusion-nonfree-release-tainted");
                            runCommand("sudo dnf swap -y ffmpeg-free ffmpeg --allowerasing");
                            if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y multimedia --setop=\"install_weak_deps=False\" --exclude=PackageKit-gstreamer-plugin");
                                runCommand("sudo dnf groupupdate -y sound-and-video");
                            } else if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y multimedia -x PackageKit-gstreamer-plugin");
                                runCommand("sudo dnf group upgrade -y sound-and-video");
                            }
                            runCommand("sudo dnf --repo=rpmfusion-nonfree-tainted install \"*-firmware\" -y");
                            if(amd == 0){
                            runCommand("sudo dnf swap -y mesa-va-drivers mesa-va-drivers-freeworld");
                            runCommand("sudo dnf swap -y mesa-vdpau-drivers mesa-vdpau-drivers-freeworld");
                            runCommand("sudo dnf swap -y mesa-va-drivers.i686 mesa-va-drivers-freeworld.i686");
                            runCommand("sudo dnf swap -y mesa-vdpau-drivers.i686 mesa-vdpau-drivers-freeworld.i686");
                            }
                            if(nv == 0){
                            runCommand("sudo dnf install -y libva-nvidia-driver");
                            }
                            if(intel == 0){
                            runCommand("sudo dnf install -y intel-media-driver");
                            }
                            runCommand("sudo dnf install -y kdenlive krita");
                            runCommand("sudo dnf install -y steam");
                            runCommand("flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo");
                            runCommand("flatpak install com.discordapp.Discord -y");
                            runCommand("flatpak install com.usebottles.bottles -y");
                            runCommand("flatpak install net.davidotek.pupgui2 -y");
                            runCommand("flatpak install net.lutris.Lutris -y");
                            runCommand("flatpak install com.obsproject.Studio -y");
                            runCommand("flatpak install org.gimp.GIMP -y");
                            syslog(LOG_INFO, "Running Option 2");
                            break;
                        case 3:
                            if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf config-manager --setopt=\"defaultyes=True\" --setopt=\"max_parallel_downloads=10\" --save");
                            } else if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf config-manager setopt defaultyes=True max_parallel_downloads=10");
                            }
                            if (strcmp(linux_distro, "fedora") == 0) {
                                runCommand("sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %%fedora).noarch.rpm");
                                runCommand("sudo dnf install -y https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %%fedora).noarch.rpm");
                                runCommand("sudo dnf config-manager --enable fedora-cisco-openh264");
                               if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                               } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                               }
                            } else if (strcmp(linux_distro, "rhel") == 0 ||
                                strcmp(linux_distro, "centos") == 0 ||  // Includes CentOS Stream
                                strcmp(linux_distro, "oracle") == 0 ||
                                strstr(linux_distro, "rhel") != NULL ||
                                strstr(linux_distro, "centos") != NULL ||
                                strstr(linux_distro, "oracle") != NULL){
                            runCommand("sudo dnf install -y --nogpgcheck https://dl.fedoraproject.org/pub/epel/epel-release-latest-$(rpm -E %%rhel).noarch.rpm");
                            runCommand("sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %%rhel).noarch.rpm");
                            runCommand("sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-$(rpm -E %%rhel).noarch.rpm");
                            if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                            } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                            }
                            }
                            runCommand("sudo dnf install -y rpmfusion-free-release-tainted");
                            runCommand("sudo dnf install -y rpmfusion-nonfree-release-tainted");
                            runCommand("sudo dnf swap -y ffmpeg-free ffmpeg --allowerasing");
                            if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y multimedia --setop=\"install_weak_deps=False\" --exclude=PackageKit-gstreamer-plugin");
                                runCommand("sudo dnf groupupdate -y sound-and-video");
                            } else if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y multimedia -x PackageKit-gstreamer-plugin");
                                runCommand("sudo dnf group upgrade -y sound-and-video");
                            }
                            runCommand("sudo dnf --repo=rpmfusion-nonfree-tainted install \"*-firmware\" -y");
                            if(amd == 0){
                            runCommand("sudo dnf swap -y mesa-va-drivers mesa-va-drivers-freeworld");
                            runCommand("sudo dnf swap -y mesa-vdpau-drivers mesa-vdpau-drivers-freeworld");
                            runCommand("sudo dnf swap -y mesa-va-drivers.i686 mesa-va-drivers-freeworld.i686");
                            runCommand("sudo dnf swap -y mesa-vdpau-drivers.i686 mesa-vdpau-drivers-freeworld.i686");
                            }
                            if(nv == 0){
                            runCommand("sudo dnf install -y libva-nvidia-driver");
                            }
                            if(intel == 0){
                            runCommand("sudo dnf install -y intel-media-driver");
                            }
                            runCommand("sudo dnf install hyprland xdg-desktop-portal-hyprland");
                            runCommand("flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo");
                            syslog(LOG_INFO, "Running Option 3");
                            break;
                        case 4:
                            if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf config-manager --setopt=\"defaultyes=True\" --setopt=\"max_parallel_downloads=10\" --save");
                            } else if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf config-manager setopt defaultyes=True max_parallel_downloads=10");
                            }
                            if (strcmp(linux_distro, "fedora") == 0) {
                                runCommand("sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %%fedora).noarch.rpm");
                                runCommand("sudo dnf install -y https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %%fedora).noarch.rpm");
                                runCommand("sudo dnf config-manager --enable fedora-cisco-openh264");
                               if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                               } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                               }
                            } else if (strcmp(linux_distro, "rhel") == 0 ||
                                strcmp(linux_distro, "centos") == 0 ||  // Includes CentOS Stream
                                strcmp(linux_distro, "oracle") == 0 ||
                                strstr(linux_distro, "rhel") != NULL ||
                                strstr(linux_distro, "centos") != NULL ||
                                strstr(linux_distro, "oracle") != NULL){
                            runCommand("sudo dnf install -y --nogpgcheck https://dl.fedoraproject.org/pub/epel/epel-release-latest-$(rpm -E %%rhel).noarch.rpm");
                            runCommand("sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %%rhel).noarch.rpm");
                            runCommand("sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-$(rpm -E %%rhel).noarch.rpm");
                            if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                            } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                            }
                            }
                            runCommand("sudo dnf install -y rpmfusion-free-release-tainted");
                            runCommand("sudo dnf install -y rpmfusion-nonfree-release-tainted");
                            if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                            } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                            }
                            runCommand("sudo dnf install -y snapd");
                            runCommand("sudo dnf swap -y ffmpeg-free ffmpeg --allowerasing");
                            if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf config-manager --setopt=\"defaultyes=True\" --setopt=\"max_parallel_downloads=10\" --save");
                            } else if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y multimedia setopt install_weak_deps=False --exclude=PackageKit-gstreamer-plugin");
                                runCommand("sudo dnf group upgrade -y sound-and-video");
                            };
                            runCommand("sudo dnf groupupdate -y multimedia --setop=\"install_weak_deps=False\" --exclude=PackageKit-gstreamer-plugin");
                            runCommand("sudo dnf groupupdate -y sound-and-video");
                            if(amd == 0){
                            runCommand("sudo dnf swap -y mesa-va-drivers mesa-va-drivers-freeworld");
                            runCommand("sudo dnf swap -y mesa-vdpau-drivers mesa-vdpau-drivers-freeworld");
                            runCommand("sudo dnf swap -y mesa-va-drivers.i686 mesa-va-drivers-freeworld.i686");
                            runCommand("sudo dnf swap -y mesa-vdpau-drivers.i686 mesa-vdpau-drivers-freeworld.i686");
                            }
                            if(nv == 0){
                            runCommand("sudo dnf install -y libva-nvidia-driver");
                            }
                            if(intel == 0){
                            runCommand("sudo dnf install -y intel-media-driver");
                            }
                            runCommand("sudo dnf copr enable -y burningpho3nix/Setup-Tool");
                            runCommand("sudo dnf copr enable -y burningpho3nix/private-projects");
                            runCommand("sudo dnf install -y steam-launch setup-tool setup-tool-beta");
                            runCommand("sudo dnf install -y steam kdenlive krita rpmdevtools rpmlint 0ad chromium cool-retro-term corectrl cpu-x gnome-disk-utility elisa-player goverlay gparted mangohud piper virt-manager wine winetricks wireshark xwaylandvideobridge btop gh zsh");
                            runCommand("sudo dnf swap -y kwrite kate");
                            runCommand("sudo dnf remove -y libreoffice*");
                            runCommand("sudo ln -s /var/lib/snapd/snap /snap");
                            runCommand("chsh -s $(which zsh)");
                            runCommand("flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo");
                            runCommand("flatpak install flathub com.discordapp.Discord com.usebottles.bottles net.davidotek.pupgui2 net.lutris.Lutris com.obsproject.Studio org.gimp.GIMP org.libreoffice.LibreOffice com.google.AndroidStudio io.github.prateekmedia.appimagepool -y");
                            runCommand("flatpak install flathub org.audacityteam.Audacity de.haeckerfelix.AudioSharing org.blender.Blender de.bforartists.Bforartists com.feaneron.Boatswain org.gnome.Boxes org.gnome.clocks dev.vencord.Vesktop app.drey.EarTag -y");
                            runCommand("flatpak install flathub im.riot.Riot de.philippun1.turtle org.gnome.design.Emblem org.gnome.Firmware io.github.giantpinkrobots.flatsweep org.gnome.NetworkDisplays org.godotengine.Godot io.gitlab.gregorni.Letterpress -y");
                            runCommand("flatpak install flathub org.gnome.design.Lorem io.missioncenter.MissionCenter com.nextcloud.desktopclient.nextcloud org.prismlauncher.PrismLauncher com.github.Matoking.protontricks org.rncbc.qpwgraph org.sauerbraten.Sauerbraten -y");
                            runCommand("flatpak install flathub gg.tesseract.Tesseract org.signal.Signal org.nickvision.tagger com.github.IsmaelMartinez.teams_for_linux com.teamspeak.TeamSpeak org.jitsi.jitsi-meet org.mozilla.Thunderbird -y");
                            runCommand("flatpak install flathub com.mastermindzh.tidal-hifi com.vscodium.codium org.kde.ktorrent io.github.zen_browser.zen dev.zed.Zed com.jeffser.Alpaca -y");
                            runCommand("sudo snap install powershell --classic");
                            runCommand("stcc-start");
                            syslog(LOG_INFO, "Running Option 4");
                           break;
                        case 5:
                            if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf config-manager --setopt=\"defaultyes=True\" --setopt=\"max_parallel_downloads=10\" --save");
                            } else if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf config-manager setopt defaultyes=True max_parallel_downloads=10");
                            }
                            if (strcmp(linux_distro, "fedora") == 0) {
                                runCommand("sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %%fedora).noarch.rpm");
                                runCommand("sudo dnf install -y https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %%fedora).noarch.rpm");
                                runCommand("sudo dnf config-manager --enable fedora-cisco-openh264");
                               if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                               } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                               }
                            } else if (strcmp(linux_distro, "rhel") == 0 ||
                                strcmp(linux_distro, "centos") == 0 ||  // Includes CentOS Stream
                                strcmp(linux_distro, "oracle") == 0 ||
                                strstr(linux_distro, "rhel") != NULL ||
                                strstr(linux_distro, "centos") != NULL ||
                                strstr(linux_distro, "oracle") != NULL){
                            runCommand("sudo dnf install -y --nogpgcheck https://dl.fedoraproject.org/pub/epel/epel-release-latest-$(rpm -E %%rhel).noarch.rpm");
                            runCommand("sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %%rhel).noarch.rpm");
                            runCommand("sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-$(rpm -E %%rhel).noarch.rpm");
                            if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y core");
                            } else if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y core");
                            }
                            }
                            runCommand("sudo dnf install -y rpmfusion-free-release-tainted");
                            runCommand("sudo dnf install -y rpmfusion-nonfree-release-tainted");
                            runCommand("sudo dnf swap -y ffmpeg-free ffmpeg --allowerasing");
                            if (strcmp(version, "39") == 0 || strcmp(version, "40") == 0 || strcmp(version, "9") == 0 || strcmp(version, "10") == 0) {
                                runCommand("sudo dnf groupupdate -y multimedia --setop=\"install_weak_deps=False\" --exclude=PackageKit-gstreamer-plugin");
                                runCommand("sudo dnf groupupdate -y sound-and-video");
                            } else if (strcmp(version, "41") == 0) {
                                runCommand("sudo dnf group upgrade -y multimedia -x PackageKit-gstreamer-plugin");
                                runCommand("sudo dnf group upgrade -y sound-and-video");
                            }
                            if(amd == 0){
                            runCommand("sudo dnf swap -y mesa-va-drivers mesa-va-drivers-freeworld");
                            runCommand("sudo dnf swap -y mesa-vdpau-drivers mesa-vdpau-drivers-freeworld");
                            runCommand("sudo dnf swap -y mesa-va-drivers.i686 mesa-va-drivers-freeworld.i686");
                            runCommand("sudo dnf swap -y mesa-vdpau-drivers.i686 mesa-vdpau-drivers-freeworld.i686");
                            }
                            if(nv == 0){
                            runCommand("sudo dnf install -y libva-nvidia-driver");
                            }
                            if(intel == 0){
                            runCommand("sudo dnf install -y intel-media-driver");
                            }
                            runCommand("sudo dnf --repo=rpmfusion-nonfree-tainted install \"*-firmware\" -y");
                            runCommand("sudo dnf remove -y libreoffice*");
                            runCommand("flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo");
                            runCommand("flatpak install flathub org.libreoffice.LibreOffice -y");
                            syslog(LOG_INFO, "Running Option 5");
                            break;
                        case 6:
                            printf("Returning to main menu.\n");
                            syslog(LOG_INFO, "Return to main menu");
                            break;
                        default:
                            printf("Invalid option. Please try again.\n");
                            break;
                    }
                    if (script_option == 6) {
                        break;
                    }
                }
                break;
            }
            case 6:{
                int sys_option;
                while (1) {
                    printf("\n");
                    printf("Choose an option for system info:\n");
                    printf("1. Short, Quick Overview\n");
                    printf("2. PCI/PCIE Devices\n");
                    printf("3. USB Devices\n");
                    printf("4. Storage Devices\n");
                    printf("5. Back to main menu\n");
                    printf("Option: ");
                    sys_option = getMenuOption(5);

                    syslog(LOG_INFO, "Displaying system overview menu");

                    switch (sys_option) {
                        case 1:
                            if (ffi == 0) {
                                runCommand("FFTS_IGNORE_PARENT=1 fastfetch");
                                syslog(LOG_INFO, "Running Option 1 with fastfetch");
                            } else if (strcmp(linux_distro, "rhel") == 0 ||
                                    strcmp(linux_distro, "centos") == 0 ||  // Includes CentOS Stream
                                    strcmp(linux_distro, "oracle") == 0 ||
                                    strstr(linux_distro, "rhel") != NULL ||
                                    strstr(linux_distro, "centos") != NULL ||
                                    strstr(linux_distro, "oracle") != NULL &&
                                    ffi != 0){
                                        int nfi = system("neofetch --version");

                                        if (nfi == 0){
                                            runCommand("neofetch");
                                        } else if (nfi != 0){
                                            runCommand("sudo dnf install -y neofetch");
                                            runCommand("neofetch");
                                        }
                                    syslog(LOG_INFO, "Running Option 1 with neofetch");
                                } else {
                                    runCommand("sudo dnf install -y fastfetch");
                                    runCommand("FFTS_IGNORE_PARENT=1 fastfetch");
                                    syslog(LOG_INFO, "Running Option 1 with fastfetch");
                                }
                            break;
                        case 2:
                            runCommand("lspci -nnk");
                            syslog(LOG_INFO, "Running Option 2");
                            break;
                        case 3:
                            runCommand("lsusb");
                            syslog(LOG_INFO, "Running Option 3");
                            break;
                        case 4:
                            runCommand("lsblk -fa");
                            syslog(LOG_INFO, "Running Option 4");
                            break;
                        case 5:
                            printf("Returning to main menu.\n");
                            syslog(LOG_INFO, "Return to main menu");
                            break;
                        default:
                            printf("Invalid option. Please try again.\n");
                            break;
                    }

                    if (sys_option == 5) {
                        break;
                    }
                }
                break;
            }
            case 7:{
                if (di == 0 || fi == 0 || si == 0) {
                    if (si == 0) {
                        runCommand("sudo snap refresh");
                    }
                    if (di == 0) {
                        runCommand("sudo dnf -y update --refresh");
                    }
                    if (fi == 0) {
                        runCommand("flatpak update -y");
                    }
                    syslog(LOG_INFO, "Updating...");
                } else {
                    printf("DNF, Flatpak, and Snap are not installed.\n");
                }
                break;
            }
            case 8:
                if (strcmp(version, "39") == 0) {
                    int choice;

                    while (1) {
                    printf("\n");
                    printf("Upgrade options:\n");
                    printf("1. Upgrade to Fedora 40\n");
                    printf("2. Upgrade to Fedora 41\n");
                    printf("3. Go back\n");
                    printf("Enter your choice: ");
                    choice = getMenuOption(3);

                    syslog(LOG_INFO, "Displaying upgrade menu");

                    switch (choice) {
                        case 1:
                            runCommand("sudo dnf --refresh upgrade");
                            runCommand("sudo dnf -y system-upgrade download --releasever=40"); // Change to 40
                            runCommand("sudo dnf -y system-upgrade reboot");
                            syslog(LOG_INFO, "Upgrade by one version");
                            break;
                        case 2:
                            runCommand("sudo dnf --refresh upgrade");
                            runCommand("sudo dnf -y system-upgrade download --releasever=41");
                            runCommand("sudo dnf -y system-upgrade reboot");
                            syslog(LOG_INFO, "Upgrade by two versions");
                            break;
                        case 3:
                            printf("Returning to main menu.\n");
                            syslog(LOG_INFO, "Return to main menu");
                            break;
                        default:
                            printf("Invalid choice.\n");

                        if (choice == 3) {
                            break;
                        }
                    }
                  }
                } else if (strcmp(version, "40") == 0) {
                    runCommand("sudo dnf --refresh upgrade");
                    runCommand("sudo dnf -y system-upgrade download --releasever=41");
                    runCommand("sudo dnf -y system-upgrade reboot");
                    syslog(LOG_INFO, "Upgrading 40 to 41");
                } else if (strcmp(version, "41") == 0){
                    printf("You are already on the latest version. \n");
                    printf("Do want to look for updates instead? (Y/n): ");

                    getchar();

                    char wantUpdate;
                    scanf("%c", &wantUpdate);

                    syslog(LOG_INFO, "Is on latest");

                    if (wantUpdate == 'y' || wantUpdate == 'Y' || wantUpdate == '\n') {
                        if (si == 0) {
                            runCommand("sudo snap refresh");
                       }
                        if (di == 0) {
                        runCommand("sudo dnf -y update --refresh");
                        }
                        if (fi == 0) {
                            runCommand("flatpak update -y");
                        }
                        syslog(LOG_INFO, "Updating...");
                    } else {
                        printf("No updates will be checked.\n");
                    }
                } else if (strcmp(version, "9") == 0){
/*
                    runCommand("subscription-manager repos --enable rhel-9-for-x86_64-baseos-rpms --enable rhel-9-for-x86_64-appstream-rpms");
                    runCommand("sudo dnf update -y");
                    runCommand("sudo dnf install -y leapp-repository leapp leapp-upgrade");
                    runCommand("sudo dnf install -y leapp-upgrade-el9to10");
                    runCommand("sudo dnf install -y cockpit-leapp");
                    runCommand("sudo leapp preupgrade");
                    printf("\n");
                    printf("Now answer the questions in /var/log/leapp/answerfile\n");
                    printf("Enter \"GO\" after answering the questions: ");
                    scanf(" %[^\n]", answer);
                    if (strcmp(answer, "GO") == 0){
                    runCommand("sudo leapp upgrade");
                    printf("Run \"cat /etc/redhat-release\" after the reboot to check if the upgrade was successful.");
                    sleep(15);
                    runCommand("sudo reboot");
                    }
                } else if (strcmp(version, "10") == 0){
*/
                    printf("You are already on the latest version.\n");
                    printf("Do want to look for updates instead? (Y/n): ");

                    getchar();

                    char wantUpdate;
                    scanf("%c", &wantUpdate);

                    if (wantUpdate == 'y' || wantUpdate == 'Y' || wantUpdate == '\n') {
                        if (si == 0) {
                            runCommand("sudo snap refresh");
                       }
                        if (di == 0) {
                            runCommand("sudo dnf -y update --refresh");
                        }
                        if (fi == 0) {
                            runCommand("flatpak update -y");
                        }
                        syslog(LOG_INFO, "Updating...");
                    } else {
                        printf("No updates will be checked.\n");
                    }
                } else {
                    printf("This program can't find any version upgrades for you,\n");
                    printf("you can still try:\n");
                    printf("sudo dnf --refresh upgrade && sudo dnf -y system-upgrade download --releasever=[your version plus 2]\n");
                    syslog(LOG_INFO, "How did you get this installed???");
                    break;
                }
                break;
            case 9:
                syslog(LOG_INFO, "Rebooting");
                syslog(LOG_INFO, "-----END OF LOG-----");
                syslog(LOG_INFO, " ");
                closelog(); // Close the log
                runCommand("sudo touch /tmp/setup-tool-$(date +%%Y-%%m-%%d).log");
                runCommand("sudo chmod 777 /tmp/setup-tool-$(date +%%Y-%%m-%%d).log");
                runCommand("sudo journalctl -t setup-tool -S today -U now > /tmp/setup-tool-$(date +%%Y-%%m-%%d).log");
                runCommand("systemctl reboot");
                break;
            case 10:
                if (stcs == 0) {
                    Script scripts[MAX_SCRIPTS];
                    int scriptCount = 0;

                    // Check for packaged scripts
                    int packagedScriptCount = listScripts("/opt/setup-tool/custom-scripts", scripts, scriptCount, MAX_SCRIPTS);
                    if (packagedScriptCount < 0) {
                        printf("Error loading packaged scripts\n");
                    } else {
                        scriptCount = packagedScriptCount;
                    }

                // Check for community scripts if we have space
                if (scriptCount < MAX_SCRIPTS) {
                    char *home_dir = getenv("HOME");
                    if (home_dir != NULL) {
                        char community_scripts_path[MAX_PATH];
                        int ret = snprintf(community_scripts_path, sizeof(community_scripts_path),
                                        "%s/.local/share/setup-tool/community-scripts", home_dir);

                        if (ret > 0 && ret < sizeof(community_scripts_path)) {
                            int communityScriptCount = listScripts(community_scripts_path, scripts,
                                                                 scriptCount, MAX_SCRIPTS);
                            if (communityScriptCount >= 0) {
                                scriptCount += communityScriptCount;
                            }
                        }
                    }
                }

                // Set execute permissions (if needed)
                runCommand("sudo chmod +x /opt/setup-tool/custom-scripts/* > /dev/null 2>&1");
                char *home_dir = getenv("HOME");
                if (home_dir != NULL) {
                    char chmod_command[MAX_PATH * 2];
                    snprintf(chmod_command, sizeof(chmod_command),
                            "chmod +x %s/.local/share/setup-tool/community-scripts/* > /dev/null 2>&1",
                            home_dir);
                    runCommand(chmod_command);
                }

                if (scriptCount > 0) {
                    while (1) {
                        printf("\nAvailable scripts:\n");
                        for (int i = 0; i < scriptCount; i++) {
                            if (i < packagedScriptCount) {
                                printf("%d. %s (packaged)\n", i + 1, scripts[i].name);
                            } else {
                                printf("%d. %s (manual)\n", i + 1, scripts[i].name);
                            }
                        }
                        printf("%d. Back to main menu\n", scriptCount + 1);
                        printf("Select a script to run (1-%d): ", scriptCount + 1);

                        int choice = getMenuOption(scriptCount + 1);
                        if (choice == scriptCount + 1) {
                            break;  // Back to main menu
                        } else if (choice >= 1 && choice <= scriptCount) {
                            int result = executeScript(&scripts[choice - 1]);
                            if (result != SCRIPT_SUCCESS) {
                                printf("Error executing script (code: %d)\n", result);
                            }
                        } else {
                            printf("Invalid choice. Please try again.\n");
                        }
                    }
                } else {
                    printf("No custom or community scripts found.\n");
                }
                break;
            } else if (stcs != 0) {
                    printf("Quitting the program.\n");
                    syslog(LOG_INFO, "Closing program");
                    syslog(LOG_INFO, "-----END OF LOG-----");
                    syslog(LOG_INFO, " ");
                    closelog(); // Close the log
                    runCommand("sudo touch /tmp/setup-tool-$(date +%%Y-%%m-%%d).log");
                    runCommand("sudo chmod 777 /tmp/setup-tool-$(date +%%Y-%%m-%%d).log");
                    runCommand("sudo journalctl -t setup-tool -S today -U now > /tmp/setup-tool-$(date +%%Y-%%m-%%d).log");
                    exit(0);
                }
            case 11:
                printf("Quitting the program.\n");
                syslog(LOG_INFO, "Closing program");
                syslog(LOG_INFO, "-----END OF LOG-----");
                syslog(LOG_INFO, " ");
                closelog(); // Close the log
                runCommand("sudo touch /tmp/setup-tool-$(date +%%Y-%%m-%%d).log");
                runCommand("sudo chmod 777 /tmp/setup-tool-$(date +%%Y-%%m-%%d).log");
                runCommand("sudo journalctl -t setup-tool -S today -U now > /tmp/setup-tool-$(date +%%Y-%%m-%%d).log");
                exit(0);
            default:
                printf("Invalid option. Please try again.\n");
                break;
            }
        }
    if (stcs == 0){
        if (option == 11)
        return 0;
    } else if (stcs != 0) {
        if (option == 10)
        return 0;
    }
    }
}
