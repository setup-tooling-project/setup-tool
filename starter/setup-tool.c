#include <stdio.h>                                                                              // libraries
#include <stdlib.h>                                                                             // that
#include <string.h>                                                                             // are
#include <sys/syslog.h>
#include <unistd.h>                                                                             // needed
#include <termios.h>                                                                            // for
#include <stdbool.h>                                                                            // this
#include <fcntl.h>                                                                              // to run
#include <time.h>
#include <sys/wait.h>
#include <syslog.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/stat.h>

#include "/usr/lib64/libstp/libstp-c.h"


#define PROGRAMS_PATH "/usr/bin/"

#define CLI 0
#define TUI 1
#define GUI 2
#define Menu 3

int main(int argc, char *argv[]) {

    if (argc == 2 && strcmp(argv[1], "--version") == 0 || argc == 2 && strcmp(argv[1], "-v") == 0) {
        int stcs = system("rpm -q st-community-scripts > /dev/null 2>&1");

        runCommand("rpm -q setup-tool");
        printf("\n");
        runCommand("rpm -q setup-tool-cli");
        printf("\n");
        if (stcs == 0) {
            runCommand("rpm -q st-community-scripts");
            printf("\n");
        }
        runCommand("rpm -q libstp");
        printf("\n");
        runCommand("rpm -q libstp-extras");
        printf("\n");
    } else if (argc == 2 && strcmp(argv[1], "--arch") == 0 || argc == 2 && strcmp(argv[1], "-a") == 0) {
       runCommand("uname -m");
    } else if (argc == 2 && strcmp(argv[1], "--make-script-directory") == 0 || argc == 2 && strcmp(argv[1], "-msd") == 0) {
       const char *home_dir = getenv("HOME");
        if (home_dir == NULL) {
            fprintf(stderr, "Error: Unable to determine home directory.\n");
            return 1;
        }

        char script_dir[MAX_PATH];
        int result = snprintf(script_dir, sizeof(script_dir), "%s/.local/share/setup-tool/community-scripts", home_dir);
        if (result < 0 || result >= (int)sizeof(script_dir)) {
            fprintf(stderr, "Error: Path construction failed.\n");
            return 1;
        }

        struct stat st;
        if (stat(script_dir, &st) == 0) {
            printf("Directory already exists: %s\n", script_dir);
        } else {
            char mkdir_command[MAX_PATH * 2];
            snprintf(mkdir_command, sizeof(mkdir_command), "mkdir -p %s", script_dir);
            if (system(mkdir_command) == -1) {
                perror("Error creating directory");
                return 1;
            } else {
                printf("Directory created successfully: %s\n", script_dir);
            }
        }
    } else if (argc == 2 && strcmp(argv[1], "--help") == 0 || argc == 2 && strcmp(argv[1], "-h") == 0) {
        printf("Setup Tool help page\n");
        printf("\n");
        printf("Usage:\n");
        printf("\n");
        printf("--version/-v                    | for the version\n");
        printf("--arch/-a                       | for your CPU architecture\n");
        printf("--make-script-directory/-msd    | to make the script directory (~/.local/share/setup-tool/community-scripts)\n");
        printf("--help/-h                       | for this help page\n");
        printf("\n");
        printf("If used without flags it will start normal\n");
        printf("\n");
    } else {

        setConfigFile("/etc/setup-tool.conf");

        bool shouldAutostart = true;

        display_special_day();

        if (shouldAutostart) {
            autostart_beta();
        }

    const char* programs[] = {"setup-tool-cli", "setup-tool-tui", "setup-tool-gui"};
    int numPrograms = sizeof(programs) / sizeof(programs[0]);

    int installedPrograms[numPrograms];
    int numInstalledPrograms = 0;  // Track the number of installed programs

    for (int i = 0; i < numPrograms; i++) {
        installedPrograms[i] = checkProgramExists(programs[i]);
        if (installedPrograms[i]) {
            numInstalledPrograms++;
        }
    }

    if (numInstalledPrograms == 0) {
        printf("No installed programs found.\n");
        return 0;
    }

    if (shouldShowNotice_beta()) {
        printf("This version of the Setup Tool is still not final.\n");
        printf("\n");
    }

    if (shouldShowNotice_tp()) {
        char confirm[2];
        int validInput = 0;

        while (!validInput) {
            printf("This program might pull software from third party repositories,\n");
            printf("if you enable these repositories through this program or through other means.\n");
            printf("Do you accept this? (Y/n): ");
            fgets(confirm, sizeof(confirm), stdin);
            if (confirm[0] == 'Y' || confirm[0] == 'y' || confirm[0] == '\n') {
                validInput = 1;
                saveUserResponse_tp('Y');  // Save user's "Y" response as 'False'
            } else if (confirm[0] == 'N' || confirm[0] == 'n') {
                printf("Exiting the program.\n");
                saveUserResponse_tp('N');  // Save user's "N" response as 'True'
                return 0;
            } else {
                printf("Invalid input. Please enter Y/y or Enter to confirm or N/n to exit.\n");
            }
        }
    }

    if (numInstalledPrograms == 1) {
        int programIndex = 0;
        while (!installedPrograms[programIndex]) {
            programIndex++;
        }

        char fullPath[100];
        sprintf(fullPath, "%s%s", PROGRAMS_PATH, programs[programIndex]);

        execlp(fullPath, programs[programIndex], NULL);

        perror("Failed to execute the program");
        return 1;
    }

    printf("Installed Programs:\n");
    int option = 1;

    for (int i = 0; i < numPrograms; i++) {
        if (installedPrograms[i]) {
            printf("%d. %s\n", option, programs[i]);
            option++;
        }
    }

    int choice;
    printf("Enter the number of the program to launch (or 0 to exit): ");
    scanf("%d", &choice);

    int selectedOption = 1;
    int selectedProgramIndex = -1;

    for (int i = 0; i < numPrograms; i++) {
        if (installedPrograms[i]) {
            if (selectedOption == choice) {
                selectedProgramIndex = i;
                break;
            }
            selectedOption++;
        }
    }

    if (selectedProgramIndex != -1) {
        char fullPath[100];
        sprintf(fullPath, "%s%s", PROGRAMS_PATH, programs[selectedProgramIndex]);

        execlp(fullPath, programs[selectedProgramIndex], NULL);

        perror("Failed to execute the program");
        return 1;
    }
    }
    return 0;
}
